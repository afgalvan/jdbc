-- noinspection SqlNoDataSourceInspectionForFile

CREATE TABLE genre (
    id    BIGSERIAL UNIQUE PRIMARY KEY,
    name  VARCHAR(255) NOT NULL
);

