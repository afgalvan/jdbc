#!/bin/bash

./gradlew assemble
image="andresgalvan/jdbc-app:v1.0"
docker build . -t "$image"

if [ "$1" == "push" ]; then
  docker push "$image"
fi
