#!/bin/bash
set -e

if ! [ -x "$(command -v kubectl)" ]; then
    echo 'Error: kubectl is not installed.' >&2
    exit 1
fi

kubectl apply -f k8s/db/
kubectl apply -f k8s/
