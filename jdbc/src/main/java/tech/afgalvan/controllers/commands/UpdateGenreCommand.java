package tech.afgalvan.controllers.commands;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Introspected
public class UpdateGenreCommand {
    @NotNull
    private final Long id;

    @NotBlank
    private final String name;

    public UpdateGenreCommand(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
