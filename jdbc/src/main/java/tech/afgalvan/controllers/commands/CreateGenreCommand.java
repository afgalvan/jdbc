package tech.afgalvan.controllers.commands;

import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotBlank;

@Introspected
public class CreateGenreCommand {
    @NotBlank
    private final String name;

    public CreateGenreCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
